# OmegaT adaptive theme plugin

This is OmegaT theme plugin to provide adaptive coloring for OmegaT 5.6.0 and later.
The plugin allow omegat to select dark/light, high/normal contrast themes based on DarkLaf project.


## Build system

The plugin  use a Gradle build system as same as OmegaT version 4.3.0 and later.

## Dependency

OmegaT and dependencies are located on remote mavenCentral repositories.
It is necessary to connect the internet to compile your project.

The plugin refers OmegaT 5.7.1.

## License

The plugin is distributed under GNU General Public License 3 or later.

